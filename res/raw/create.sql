CREATE TABLE term (
 id INTEGER PRIMARY KEY AUTOINCREMENT,
 name varchar(50) NOT NULL,
 description varchar(760) NOT NULL,
 favorite INTEGER NOT NULL 
);