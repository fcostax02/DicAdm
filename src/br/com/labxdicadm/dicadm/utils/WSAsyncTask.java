package br.com.labxdicadm.dicadm.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

public abstract class WSAsyncTask<Result> extends AsyncTask<Void, Void, Result> {

	private boolean error = false;

	private final Context context;
	private final ProgressDialog dialog;

	public WSAsyncTask(Context context) {
		this.context = context;
		dialog = new ProgressDialog(context);
		if (!this.dialog.isShowing()) {
			this.dialog.setMessage("Aguarde, criando termos...");
		}
	}

	@Override
	protected void onPreExecute() {
		this.dialog.show();
	}

	@Override
	protected Result doInBackground(Void... params) {
		try {
			return executeWS();
		} catch (Exception e) {
			error = true;
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(Result result) {

		if (this.dialog.isShowing()) {
			this.dialog.setMessage("Pronto! Faça novamente a busca e bom uso!");
			this.dialog.dismiss();
		}

		if (!error) {
			afterWSExecution(result);
		} else {
			// FIXME internacionalizar
			Toast.makeText(context, "Não foi possível completar a ação.",
					Toast.LENGTH_LONG).show();
		}

	}

	public abstract Result executeWS() throws Exception;

	public abstract void afterWSExecution(Result result);

}
