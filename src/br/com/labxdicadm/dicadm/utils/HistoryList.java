package br.com.labxdicadm.dicadm.utils;

import java.util.ArrayList;
import java.util.List;

import br.com.labxdicadm.dicadm.entities.Term;

public class HistoryList {

	private final static List<Term> listOfTerms = new ArrayList<Term>();

	/**
	 * Adiciona um item ao Histórico
	 * 
	 * @param entity
	 */
	public static void addTerm(Term entity) {
		if (!listOfTerms.contains(entity) && entity != null) {
			listOfTerms.add(entity);
		}
	}

	/**
	 * Retorna uma lista de termos acessados pelo usuário (Histórico)
	 * 
	 * @return list<Term>
	 */
	public static List<Term> getHistory() {
		return listOfTerms;
	}
}
