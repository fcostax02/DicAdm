package br.com.labxdicadm.dicadm.adapters;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.com.felipe.dicadm.R;
import br.com.labxdicadm.dicadm.entities.Term;

public class TermAdapter extends BaseAdapter {

	private List<Term> itens = new ArrayList<Term>();
	private LayoutInflater mInflater;

	public TermAdapter(Context context, List<Term> itens) {
		for (Term entity : itens) {
			if (!this.itens.contains(entity) && entity != null) {
				this.itens.add(entity);
			}
		}
		mInflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return itens.size();
	}

	@Override
	public Object getItem(int position) {
		return itens.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		Term item = itens.get(position);
		view = mInflater.inflate(R.layout.item_term_list, null);

		TextView tv_name = ((TextView) view.findViewById(R.id.tv_name));
		tv_name.setText(item.getName());
		tv_name.setTag(item.getDesctiption());

		return view;
	}

}
