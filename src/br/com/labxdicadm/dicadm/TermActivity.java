package br.com.labxdicadm.dicadm;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import br.com.felipe.dicadm.R;

public class TermActivity extends ActionBarActivity {

	private TextView tv_name;
	private TextView tv_description;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_term);
		initView();

		tv_name.setText(getIntent().getExtras().get("name").toString());
		tv_description.setText(getIntent().getExtras().get("description")
				.toString());
	}

	private void initView() {
		tv_name = (TextView) findViewById(R.id.tv_name_TA);
		tv_description = (TextView) findViewById(R.id.tv_description_TA);
	}

	public void backToMain(View v) {
		Intent i = new Intent();
		i.setClass(TermActivity.this, MainActivity.class);
		startActivity(i);
		finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.term, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
