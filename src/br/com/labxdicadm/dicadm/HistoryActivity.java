package br.com.labxdicadm.dicadm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import br.com.felipe.dicadm.R;
import br.com.labxdicadm.dicadm.adapters.TermAdapter;
import br.com.labxdicadm.dicadm.entities.Term;
import br.com.labxdicadm.dicadm.utils.HistoryList;

public class HistoryActivity extends ActionBarActivity {

	private ListView lv_history;
	private TermAdapter termAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_history);

		initViews();
		createListView();
	}

	private void createListView() {

		if (HistoryList.getHistory() != null
				&& !HistoryList.getHistory().isEmpty()) {
			// Coloca a lista de trás para frente.
			Collections.reverse(HistoryList.getHistory());
			termAdapter = new TermAdapter(HistoryActivity.this,
					HistoryList.getHistory());

			lv_history.setAdapter(termAdapter);
			lv_history.setCacheColorHint(Color.TRANSPARENT);
		} else {
			// Criando objeto com mensagem vazia
			Term entity = new Term();
			entity.setName("NENHUM HISTÓRICO");

			// Setando a lista vazia
			List<Term> listEmpty = new ArrayList<Term>();
			listEmpty.add(entity);

			termAdapter = new TermAdapter(HistoryActivity.this, listEmpty);
			lv_history.setAdapter(termAdapter);
			lv_history.setCacheColorHint(Color.TRANSPARENT);
		}
	}

	public void initViews() {
		lv_history = (ListView) findViewById(R.id.historyList);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.history, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
