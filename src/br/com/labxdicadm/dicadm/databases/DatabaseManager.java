package br.com.labxdicadm.dicadm.databases;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public abstract class DatabaseManager extends SQLiteOpenHelper {

	protected Context context;

	public DatabaseManager(Context context, String name, int version) {
		super(context, name, null, version);
		this.context = context;
	}

	public abstract void onCreate(SQLiteDatabase bd);

	public abstract void onUpgrade(SQLiteDatabase bd, int actualVersion,
			int newVersion);

	/**
	 * Atravez do id do arquivo sql será gerado o banco de dados.
	 *
	 * @param fileID
	 * @param bd
	 * @throws IOException
	 */
	protected void byFile(int fileID, SQLiteDatabase bd) throws IOException {
		StringBuilder sql = new StringBuilder();
		BufferedReader br = new BufferedReader(new InputStreamReader(context
				.getResources().openRawResource(fileID)));
		String line;
		while ((line = br.readLine()) != null) {
			line = line.trim();
			if (line.length() > 0) {
				sql.append(line);
				if (line.endsWith(";")) {
					bd.execSQL(sql.toString());
					sql.delete(0, sql.length());
				}
			}
		}
	}

}
