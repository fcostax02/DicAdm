package br.com.labxdicadm.dicadm.databases;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import br.com.felipe.dicadm.R;

public class DatabaseEntity extends DatabaseManager {

	// nome do banco de dados e versão
	public static final String NAME = "dicadm";
	public static final String TAG_LOG = "databaseDicadm";
	public static final int VERSION = 1;

	public DatabaseEntity(Context context) {
		// defino pelo contrutor do DatabaseManager a versão e o nome do banco
		super(context, NAME, VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase bd) {
		createTables(bd);
	}

	/**
	 * Este método é chamado automaticamente quando a versão é alterada.
	 */
	@Override
	public void onUpgrade(SQLiteDatabase bd, int actualVersion, int newVersion) {
		// realiza tratamento de upgrade, caso tenha
		// alteração em tabelas por exemplo.
		Log.e(TAG_LOG, "V.atual: " + actualVersion);
		Log.e(TAG_LOG, "Nova V.: " + newVersion);
		// Aqui você deve fazer o tratamento do update do banco.
		// no caso estou apagando minha tabela e criando novamente.
		try {
			bd.execSQL("drop table term;");
		} catch (Exception e) {
			Log.e(TAG_LOG, "onUpgrade", e);
		}
		createTables(bd);
	}

	private void createTables(SQLiteDatabase bd) {
		try {
			// crio o banco de dados atravez do arquivo create.
			byFile(R.raw.create, bd);
		} catch (Exception e) {
			Log.e(TAG_LOG, "createTables", e);
		}
	}

}
