package br.com.labxdicadm.dicadm.databases;

import android.database.sqlite.SQLiteDatabase;

public class Database {
	
	private DatabaseManager databaseManager;
    private SQLiteDatabase sqld;
 
    public Database(DatabaseManager DatabaseManager) {
        this.databaseManager = DatabaseManager;
    }
 
    public void open() {
        sqld = databaseManager.getWritableDatabase();
    }
 
    public SQLiteDatabase get() {
        if (sqld != null && sqld.isOpen()) {
            return sqld;
        }
        return null;
    }
 
    public void close() {
    	databaseManager.close();
    }

}
