package br.com.labxdicadm.dicadm.repositories;

import java.io.File;
import java.util.List;

import android.content.Context;
import br.com.labxdicadm.dicadm.databases.Database;
import br.com.labxdicadm.dicadm.entities.Term;

public interface TermRepository {

	void insert(List<Term> list, Database database);

	List<Term> findAll(Context context, String name, Database database, File file);

	void remove(Long id, Context context, Database database);
}
