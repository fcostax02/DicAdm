package br.com.labxdicadm.dicadm.repositories.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.os.Environment;
import android.util.Log;
import br.com.labxdicadm.dicadm.databases.Database;
import br.com.labxdicadm.dicadm.databases.DatabaseEntity;
import br.com.labxdicadm.dicadm.entities.Term;
import br.com.labxdicadm.dicadm.repositories.TermRepository;

import com.opencsv.CSVReader;

public class TermRepositoryImpl implements TermRepository {

	private final String CSV_PATH = "bd_final.csv";
	private static final String OK = "444bcb3a3fcf8389296c49467f27e1d6";

	/**
	 * Inserir
	 */
	@Override
	public void insert(List<Term> list, Database database) {
		ContentValues cv = new ContentValues();
		database.open();
		try {
			database.get().execSQL("delete from term; vacuum;");
			for (Term entity : list) {
				if (entity != null) {
					cv.put("id", entity.getId());
					cv.put("name", entity.getName());
					cv.put("description", entity.getDesctiption());
					// Se for True, salva 1, se falso salva Zero. Sqlite não tem
					// Boolean, tem que salvar um inteiro
					cv.put("favorite", entity.getFavorite() ? 1 : 0);
					database.get().insert("term", null, cv);
				}
			}
		} catch (Exception e) {
			Log.d("TermRepositoryImpl",
					"Method: insert().\nErro ao inserir dados do banco. Causa: "
							+ e.getCause());
		}
		database.close();
	}

	/**
	 * Listar todos
	 */
	@Override
	public List<Term> findAll(Context context, String name, Database database,
			File file) {
		Cursor cursor = null;
		database.open();
		if (name.equals("")) {
			try {
				cursor = database.get().rawQuery(
						"select * from term limit 20;", null);
			} catch (Exception e) {
				Log.d("TermRepositoryImpl",
						"Method: findall().\nErro ao recuperar todos os dados do banco. Causa: "
								+ e.getMessage());
			}
		} else {
			try {
				cursor = database.get().rawQuery(
						"select * from term where name like '%" + name
								+ "%' limit 15;", null);
			} catch (Exception e) {
				Log.d("TermRepositoryImpl",
						"Method: findall().\nErro ao recuperar dados do banco. Causa: "
								+ e.getMessage());
			}
		}

		List<Term> listEntity = null;
		Term entity;
		boolean haveDataDb = false;

		if (cursor != null && !cursor.isClosed()) {
			listEntity = new ArrayList<Term>();
			while (cursor.moveToNext()) {
				entity = new Term();
				entity.setId(Long.parseLong(cursor.getString(cursor
						.getColumnIndex("id"))));
				entity.setName(cursor.getString(cursor.getColumnIndex("name")));
				entity.setDesctiption(cursor.getString(cursor
						.getColumnIndex("description")));
				entity.setFavorite(cursor.getString(
						cursor.getColumnIndex("favorite")).equals("1") ? true
						: false);
				listEntity.add(entity);
				haveDataDb = true;
			}
			cursor.close();
		}

		if (!haveDataDb) {
			// Popula o banco.
			if (!verifyPopulate(database)) {
				populate(context, database);
			}
		}

		database.close();
		return listEntity;
	}

	/**
	 * Verifica se o banco foi populado ou não.
	 * 
	 * @param database
	 * @return Um boolean, se true já tem banco, se false, ainda não há.
	 */
	private Boolean verifyPopulate(Database database) {
		Cursor c = null;
		try {
			c = database.get().rawQuery("select * from term limit 20;", null);
		} catch (Exception e) {
			Log.d("TermRepositoryImpl",
					"Method: verifyPopulate().\nErro ao recuperar dados do banco. Causa: "
							+ e.getMessage());
		}
		String ok = "";
		if (c != null && !c.isClosed()) {
			if (c.moveToFirst()) {
				ok = c.getString(c.getColumnIndex("name"));
			}
			c.close();
		}
		if (ok.equals(OK)) {
			// Banco já feito.
			return true;
		} else {
			// Cria o banco.
			return false;
		}
	}

	/**
	 * Remover
	 */
	@Override
	public void remove(Long id, Context context, Database database) {
		try {
			database.open();
			// (TABELA, COLUNA, WHERE CLAUSE)
			database.get().delete("term", "id",
					new String[] { String.valueOf(id) });
		} catch (Exception e) {
			Log.d("TermRepositoryImpl",
					"Method: remove().\nErro ao remover dados do banco. Causa: "
							+ e.getMessage());
		} finally {
			database.close();
		}
	}

	// Unnecessary method.
	public static boolean isExternalStorageReadOnly() {
		String extStorageState = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState)) {
			return true;
		}
		return false;
	}

	// Unnecessary method
	public static boolean isExternalStorageAvailable() {
		String extStorageState = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {
			return true;
		}
		return false;
	}

	/**
	 * Lê um arquivo CSV e retorna uma Lista de listas.
	 * 
	 * @param context
	 * @return Retorna uma Lista com outras Listas do tipo String.
	 */
	public final List<String[]> readCsv(Context context) {
		List<String[]> questionList = new ArrayList<String[]>();
		AssetManager assetManager = context.getAssets();

		try {
			InputStream csvStream = assetManager.open(CSV_PATH);
			InputStreamReader csvStreamReader = new InputStreamReader(csvStream);
			CSVReader csvReader = new CSVReader(csvStreamReader);
			String[] line;

			// throw away the header
			csvReader.readNext();

			while ((line = csvReader.readNext()) != null) {
				questionList.add(line);
			}
			csvReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return questionList;
	}

	/**
	 * Popula o Banco se estiver vazio.
	 * 
	 * @param database
	 */
	private void populate(final Context context, Database database) {
		final ProgressDialog dialog;
		String msgCreate = "Aguarde, criando termos!\nNão desligue o celular!\n\nApós isso, pesquise novamente.";
		final String msgErro = "Ops! Algo deu errado!";
		final String msgSuccess = "Pronto! Pesquise novamente!";

		dialog = new ProgressDialog(context);
		if (!dialog.isShowing()) {
			dialog.setMessage(msgCreate);
			dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			dialog.setIndeterminate(true);
			dialog.setCancelable(false);
			dialog.show();
		}
		database.get().execSQL("delete from term; vacuum;");

		/*
		 * Thread para popular o banco com os termos.
		 */
		Thread populateThread = new Thread() {

			@Override
			public void run() {
				Database db = new Database(new DatabaseEntity(context));
				List<String[]> list = readCsv(context);

				db.open();
				for (final String[] strings : list) {
					ContentValues cv = new ContentValues();
					String[] term = Arrays.toString(strings).split(
							",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
					try {
						cv.put("name", term[0].replace("[", "")
								.replace("]", ""));
						cv.put("description",
								term[1].replace("[", "").replace("]", ""));
						// Salva Zero pois não há favoritos ainda.
						cv.put("favorite", 0);
						db.get().insert("term", null, cv);
						Log.d("TermRepositoryImpl",
								"Method: populate().\nDeu certo popular a tabela!");
					} catch (Exception e) {
						Log.d("TermRepositoryImpl",
								"Method: populate().\nErro ao inserir dados do banco.");
						e.printStackTrace();
					}
				}
				db.close();
				if (dialog.isShowing()) {
					dialog.dismiss();
				}
			}
		};
		populateThread.start();
	}
}
