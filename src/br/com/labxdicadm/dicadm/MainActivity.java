package br.com.labxdicadm.dicadm;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import br.com.felipe.dicadm.R;
import br.com.labxdicadm.dicadm.adapters.TermAdapter;
import br.com.labxdicadm.dicadm.databases.Database;
import br.com.labxdicadm.dicadm.databases.DatabaseEntity;
import br.com.labxdicadm.dicadm.entities.Term;
import br.com.labxdicadm.dicadm.repositories.TermRepository;
import br.com.labxdicadm.dicadm.repositories.impl.TermRepositoryImpl;
import br.com.labxdicadm.dicadm.utils.HistoryList;

public class MainActivity extends ActionBarActivity implements
		NavigationDrawerFragment.NavigationDrawerCallbacks {

	private ListView lv_terms;
	private TermAdapter termAdapter;
	private List<Term> listTerms;
	private Context context;
	private TextView tv_name;
	private EditText et_name;
	private TermRepository dao;
	private Database database;

	private void initViews() {
		context = MainActivity.this;
		et_name = (EditText) findViewById(R.id.et_name);
		try {
			et_name.setOnEditorActionListener(new TextView.OnEditorActionListener() {
				@Override
				public boolean onEditorAction(TextView v, int actionId,
						KeyEvent event) {
					if (actionId == EditorInfo.IME_ACTION_SEARCH) {
						if (et_name.getText().toString().length() >= 4) {
							createListView();
						} else {
							Toast.makeText(MainActivity.this,
									R.string.MA_character_warning,
									Toast.LENGTH_SHORT).show();
						}
						// Fechar o Teclado após a busca
						InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(et_name.getWindowToken(), 0);
						return true;
					}
					return false;
				}
			});
		} catch (NullPointerException npe) {
			Log.d("MainActivity\nInitViews()",
					"NullPointerException. Caused by [et_name]. Is null.");
		}
	}

	private void createListView() {
		database = new Database(new DatabaseEntity(context));
		lv_terms = (ListView) findViewById(R.id.listTerms);
		dao = new TermRepositoryImpl();

		listTerms = new ArrayList<Term>();
		listTerms = dao.findAll(MainActivity.this,
				et_name.getText().toString(), database,
				new File(getDir("dex", Context.MODE_PRIVATE), "termos.xlsx"));

		if (listTerms != null && !listTerms.isEmpty()) {
			termAdapter = new TermAdapter(MainActivity.this, listTerms);
			lv_terms.setAdapter(termAdapter);
			lv_terms.setCacheColorHint(Color.TRANSPARENT);
		} else {
			// Criando objeto com mensagem vazia
			Term t = new Term();
			t.setName("NENHUM RESULTADO");
			// Setando a lista vazia
			List<Term> listEmpty = new ArrayList<Term>();
			listEmpty.add(t);

			termAdapter = new TermAdapter(MainActivity.this, listEmpty);
			lv_terms.setAdapter(termAdapter);
			lv_terms.setCacheColorHint(Color.TRANSPARENT);
		}

		lv_terms.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				tv_name = (TextView) view.findViewById(R.id.tv_name);
				if (!tv_name.getText().toString().equals("NENHUM RESULTADO")) {
					Term entity = new Term();
					entity.setName(tv_name.getText().toString());
					entity.setDesctiption(tv_name.getTag().toString());

					HistoryList.addTerm(entity);
					HistoryList.getHistory();

					Intent i = new Intent(MainActivity.this, TermActivity.class);
					i.putExtra("name", tv_name.getText().toString());
					i.putExtra("description", tv_name.getTag().toString());
					startActivity(i);
					finish();
				}
			}
		});
	}

	/**
	 * Fragment managing the behaviors, interactions and presentation of the
	 * navigation drawer.
	 */
	private NavigationDrawerFragment mNavigationDrawerFragment;

	/**
	 * Used to store the last screen title. For use in
	 * {@link #restoreActionBar()}.
	 */
	private CharSequence mTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager()
				.findFragmentById(R.id.navigation_drawer);
		mTitle = getTitle();

		// Set up the drawer.
		mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout));
	}

	@Override
	public void onNavigationDrawerItemSelected(int position) {
		// update the main content by replacing fragments
		FragmentManager fragmentManager = getSupportFragmentManager();
		fragmentManager
				.beginTransaction()
				.replace(R.id.container,
						PlaceholderFragment.newInstance(position + 1)).commit();
	}

	public void onSectionAttached(int number) {
		switch (number) {
		case 1:
			mTitle = getString(R.string.title_section1);
			break;
		case 2:
			mTitle = getString(R.string.title_section2);
			break;
		case 3:
			mTitle = getString(R.string.title_section3);
			break;
		}
	}

	public void restoreActionBar() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(R.string.app_name);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!mNavigationDrawerFragment.isDrawerOpen()) {
			initViews();
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.
			getMenuInflater().inflate(R.menu.main, menu);
			restoreActionBar();
			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		private static final String ARG_SECTION_NUMBER = "section_number";

		/**
		 * Returns a new instance of this fragment for the given section number.
		 */
		public static PlaceholderFragment newInstance(int sectionNumber) {
			PlaceholderFragment fragment = new PlaceholderFragment();
			Bundle args = new Bundle();
			args.putInt(ARG_SECTION_NUMBER, sectionNumber);
			fragment.setArguments(args);
			return fragment;
		}

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}

		@Override
		public void onAttach(Activity activity) {
			super.onAttach(activity);
			((MainActivity) activity).onSectionAttached(getArguments().getInt(
					ARG_SECTION_NUMBER));
		}
	}
}
